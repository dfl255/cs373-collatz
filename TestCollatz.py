#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )

    # personal unit tests

    def test_range(self):
        self.assertEqual(collatz_eval(1, 1), 1)
        self.assertEqual(collatz_eval(2, 2), 2)
        self.assertEqual(collatz_eval(3, 3), 8)
        self.assertEqual(collatz_eval(4, 4), 3)
        self.assertEqual(collatz_eval(5, 5), 6)

    def test_eval_high(self):
        self.assertEqual(collatz_eval(900000, 999999), 507)

    def test_eval_reverse(self):
        self.assertEqual(collatz_eval(1000, 1), 179)

    def test_eval_range_narrow(self):
        self.assertEqual(collatz_eval(1, 999999), 525)

    def test_eval_tree(self):
        self.assertEqual(collatz_eval(1, 999999), 525)
        self.assertEqual(
            max(collatz_eval(1, 500000), collatz_eval(500000, 999999)),
            collatz_eval(1, 999999),
        )

    def test_stress(self):
        for i in range(1, 837799):
            self.assertEqual(collatz_eval(i, 837799), 525)

    def test_tree_small(self):
        self.assertEqual(
            collatz_eval(1, 4), max(collatz_eval(3, 4), collatz_eval(1, 2))
        )

    def test_tree_reverse(self):
        self.assertEqual(
            max(collatz_eval(5, 1), collatz_eval(10, 5)), collatz_eval(10, 1)
        )

    def test_running_out(self):
        self.assertEqual(collatz_eval(30, 555), collatz_eval(555, 30))

    def test_of_ideas(self):
        self.assertEqual(collatz_eval(5, 5) + 1, collatz_eval(10, 10))


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
