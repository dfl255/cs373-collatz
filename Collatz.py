#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from typing import IO, List


history = []

twoPow = {
    2: 1,
    4: 2,
    8: 3,
    16: 4,
    32: 5,
    64: 6,
    128: 7,
    256: 8,
    512: 9,
    1024: 10,
    2048: 11,
    4096: 12,
    8192: 13,
    16384: 14,
    32768: 15,
    65536: 16,
    131072: 17,
    262144: 18,
    524288: 19,
    1048576: 20,
    2097152: 21,
    4194304: 22,
    8388608: 23,
    16777216: 24,
    33554432: 25,
    67108864: 26,
    134217728: 27,
    268435456: 28,
    536870912: 29,
    1073741824: 30,
}


# precalculate individual 1mil
cache = {}
for x in range(1, 1000001):
    curr = x
    length = 1
    while curr > 1:
        if curr in cache:
            # every cached number includes the intial 1 from the very last step
            # remove this to avoid doubling up
            length += cache[curr] - 1
            break
        length += 1
        if curr % 2 == 0:
            curr = curr // 2
        else:
            curr = curr + curr // 2 + 1
            length += 1
    assert length >= 1
    cache[x] = length
    assert x in cache

    # cache[i] = length;

    # history = []
# populate the segment tree
seg_tree = [0] * 1000000 * 5

# recursively populate the upper levels of segTree
def populate(index, start, end):
    assert end <= len(cache)
    if start == end:
        assert start in cache
        seg_tree[index] = cache[start]
        return
    mid = (start + end) // 2
    assert mid < end
    left = index * 2 + 1
    right = left + 1
    assert left < len(seg_tree) and right < len(seg_tree)
    populate(left, start, mid)
    populate(right, mid + 1, end)
    assert index <= len(seg_tree)
    seg_tree[index] = max(seg_tree[left], seg_tree[right])
    assert seg_tree[index] >= 1


populate(0, 1, 1000000)

# query low bound, high bound, cache range lower, upper
def query(qlower, qupper, start, end, index):

    # in case out of bounds
    if index >= len(seg_tree):
        return 0
    if qlower <= start and qupper >= end:
        assert seg_tree[index] != 0
        return seg_tree[index]
    if start > qupper or end < qlower:
        return 0

    l_index = 2 * index + 1
    r_index = 2 * index + 2
    assert l_index < len(seg_tree) and r_index < len(seg_tree)
    mid = (start + end) // 2
    left = query(qlower, qupper, start, mid, l_index)
    right = query(qlower, qupper, mid + 1, end, r_index)
    assert left >= 0 and right >= 0
    return max(left, right)


# ------------
# collatz_read
# ------------


def collatz_read(s: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]


# ------------
# collatz_eval
# ------------


def collatz_eval(i: int, j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    # <your code>
    # return i + j
    if i > j:
        i, j = j, i

    assert i <= j

    m = j // 2 + 1
    if i < m:
        i = m

    assert m <= j

    return query(i, j, 1, 1000000, 0)


# -------------
# collatz_print
# -------------


def collatz_print(w: IO[str], i: int, j: int, v: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")


# -------------
# collatz_solve
# -------------


def collatz_solve(r: IO[str], w: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
