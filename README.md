# CS373: Software Engineering Collatz Repo

* Name: David Lai

* EID: dfl434

* GitLab ID: dfl255

* HackerRank ID: David_lai_255

* Git SHA: 23cac079bca188e1473bb07d5bcaac94fa84b3a4

* GitLab Pipelines: https://gitlab.com/dfl255/cs373-collatz/pipelines

* Estimated completion time: 15 hours

* Actual completion time: 12 hours

* Comments: I'm currently taking Downing's OOP/CS371P class, which has a very similar Collatz project as well (basically this but in C++). While working, some of my earlier commits were essentially the C++ code I wrote coat the same time, but translated into Python. However, my final submission is significantly different from my final C++ code since the Python implementation had timeout issues (which I resolved by implementing a segment tree).
